# cleans up stale (old beta) nuget packages from the neftest package registry

$packagesEndpoint = "https://gitlab.com/api/v4/projects/33834162/packages/"
$headers = @{
    'PRIVATE-TOKEN' = $Env:GITLAB_PACKAGE_REPO_ACCESS_TOKEN
}

function Download-Packages-Page([int]$page, [bool]$recurse) {
	#Write-Host "Downloading Page: $page"
	$_foundPackages = @()

	$_response = Invoke-WebRequest -Headers $headers -Uri "$packagesEndpoint`?package_type=nuget&per_page=100&page=$page"
	$_responseJson = ConvertFrom-Json $([String]::new($_response.Content))

	foreach ($package in $_responseJson) {
		#Write-Host  "    Package Name is:" $package.name $package.version
		$_foundPackages += $package
	}

	if ($recurse) {
		for ($page=2; $page -le [Int]$_response.Headers['x-total-pages']; $page++) {
			$_foundPackages += Download-Packages-Page $page
		}
	}

	return $_foundPackages
}


$latestPackageVersions = @{}
$uniquePackages = @()
$allPackages = Download-Packages-Page 1 1

Write-Host "Packages: "

foreach ($package in $allPackages) {
	if ($package.version.Contains("-")) {
		Write-Host "   Found (pre) package:" $package.name $package.Version
	}
	else {
		Write-Host "   Found (release) package:" $package.name $package.Version
		if (!($latestPackageVersions.ContainsKey($package.name)) -or [System.Version]$package.version -gt $latestPackageVersions[$package.name]) {
			$latestPackageVersions[$package.name] = $package.version
		}
	}
}

Write-Host "Found" $allPackages.Count

foreach ($latest in $latestPackageVersions.GetEnumerator()) {
	Write-Host "  Found Latest: " $latest.Key $latest.Value

	foreach ($package in $allPackages) {
		if ($package.version.Contains("-") -and $package.name -eq $latest.Key) {
			if ([System.Version]($package.Version -split "-")[0] -le [System.Version]$latest.Value) {
				Write-Host "     DELETING:" $package.name $package.version
				$packageid=$package.id
				$_res = Invoke-WebRequest -Headers $headers -Method "DELETE" -Uri "$packagesEndpoint$packageid"
			}
			else {
				Write-Host "     KEEPING:" $package.name $package.version
			}
		}
	}
}
