﻿# updates nuspec files with version / release notes

#needed variables:
# $GV_SEMVER

$CHANGELOG_FILE = "vNext"

if ($CI_COMMIT_BRANCH -eq "release") {
    $CHANGELOG_FILE = "v${Env:GV_SEMVER}"
}

$vChanges=(Get-Content -path .\changelog\${CHANGELOG_FILE}.md -Raw);

$files = Get-ChildItem "." -Filter "*.nuspec"
$assetLinks = @()

Write-Host "Version:" $Env:GV_SEMVER
Write-Host "Changelog: `r`n${vChanges}"

foreach ($f in $files) {
    Write-Host "Found File:" $f.FullName
    Set-Content $f.FullName ((Get-Content -path $f.FullName -Raw) -replace '\$RELEASENOTES\$',"$vChanges")
    Set-Content $f.FullName ((Get-Content -path $f.FullName -Raw) -replace '\$VERSION\$',"${Env:GV_SEMVER}")
}