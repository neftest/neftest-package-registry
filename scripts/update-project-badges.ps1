# update test results project badge

$color = 'critical'
$message = 'not set up'

try {
    $req = Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/test_report_summary"
    $testresults = $req.Content | ConvertFrom-Json

    if ($testresults.total.count/$testresults.total.success -eq 1) {
        $color = 'success'
        $message = "✓ $($testresults.total.success)"
    }
    else {
        $color = 'critical'
        $message = "✓ $($testresults.total.success) | ✗ $($testresults.total.failed)"
    }
}
catch {
    Write-Host $_
}

Write-Host "Badge Color: ${color}"
Write-Host "Badge Message: ${message}"

$req = Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/badges?name=Test%20Results"

$badge = $req | ConvertFrom-Json


$_headers = @{
    'PRIVATE-TOKEN' = $Env:BOT_API_TOKEN
}

$jsonBody = @{
    name = "Test Results"
    image_url = "https://img.shields.io/badge/tests-${message}-${color}"
    link_url = "https://gitlab.com/${CI_PROJECT_PATH}/-/pipelines/${CI_PIPELINE_ID}/test_report"
}


Write-Host "Badge Name: $($jsonBody.name)"
Write-Host "Badge Image Url: $($jsonBody.image_url)"
Write-Host "Badge Link: $($jsonBody.link_url)"

$jsonBody = [System.Text.Encoding]::UTF8.GetBytes(($jsonBody | ConvertTo-Json))

if ($badge.name -eq "Test Results") {
    $req = Invoke-WebRequest -Headers $_headers -Method Put -ContentType "application/json" -Body $jsonBody -Uri "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/badges/$($badge.id)"
}
else {
    $req = Invoke-WebRequest -Headers $_headers -Method Post -ContentType "application/json" -Body $jsonBody -Uri "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/badges/"
}

Write-Host "Updated test result badge for ${CI_PROJECT_PATH}"
