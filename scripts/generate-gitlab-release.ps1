﻿# generates a gitlab release from the current release branch

#needed variables:
# $Env:GV_SEMVER
# $CI_PROJECT_ID
# $CI_PROJECT_TITLE
# $CI_PROJECT_PATH_SLUG
# $CI_PROJECT_PATH
# $Env:DISCORD_WEBHOOK
# $GITLAB_PACKAGE_REPO_ACCESS_TOKEN
#
#optional:
# $Env:SHOW_NUGET_LINK   # set to 1 to include nuget package links TODO: need a way to mark repos with package repositories to link to

$Env:SHOW_NUGET_LINK = 1

$packagesEndpoint = "https://gitlab.com/api/v4/projects/33834162/packages/"
$packagesDir = ".\bin\packages\"
$headers = @{
    'PRIVATE-TOKEN' = $Env:GITLAB_PACKAGE_REPO_ACCESS_TOKEN
}

function Download-Packages([string]$packageName, [int]$page, [bool]$recurse) {
	#Write-Host "Downloading Page: $page"
	$_foundPackages = @()

	$_response = Invoke-WebRequest -Headers $headers -Uri "$packagesEndpoint`?package_type=nuget&per_page=100&page=$page&package_name=$packageName"
	$_responseJson = ConvertFrom-Json $([String]::new($_response.Content))

	foreach ($package in $_responseJson) {
		#Write-Host  "    Package Name is:" $package.name $package.version
		$_foundPackages += $package
	}

	if ($recurse) {
		for ($page=2; $page -le [Int]$_response.Headers['x-total-pages']; $page++) {
			$_foundPackages += Download-Packages "$packageName" $page
		}
	}

	return $_foundPackages
}

function Download-Package-Files([string]$packageId, [int]$page, [bool]$recurse) {
	#Write-Host "Downloading PackageFiles:" $packageId
	$_foundFiles = @()

	$_response = Invoke-WebRequest -Headers $headers -Uri "$packagesEndpoint$packageId/package_files`?per_page=100&page=$page"
	$_responseJson = ConvertFrom-Json $([String]::new($_response.Content))

	foreach ($packageFile in $_responseJson) {
		$_foundFiles += $packageFile
	}

	if ($recurse) {
		for ($page=2; $page -le [Int]$_response.Headers['x-total-pages']; $page++) {
			$_foundFiles += Download-Package-Files $packageName $page
		}
	}

	return $_foundFiles
}

function Find-Latest-Package-Version-File([string]$packageName, [string]$packageVersion) {
    $allPackages = Download-Packages $packageName 1 1
    $foundPackage = 0;

    foreach ($package in $allPackages) {
	    if ($package.version.Equals($packageVersion) -and $package.name.Equals($packageName)) {
		    Write-Host "  Found package:" $package.name $package.Version
            $foundPackage = $package;
            break
	    }
    }

    if ($foundPackage -eq 0) {
        throw "Could not find package for $packageName $packageVersion"
    }
    
    $packageFiles = Download-Package-Files $package.id 1
    $foundFile = $packageFiles[0];

    foreach ($file in $packageFiles) {
	    if ([int]${file.id} -gt [int]${foundFile.id}) {
            $foundFile = $file;
	    }
    }

    if ($foundFile -eq 0) {
        throw "Could not find latest file for $packageName $packageVersion"
    }

	Write-Host "  Found file:" $foundFile.id $foundFile.file_name

    return $foundFile
}

$files = Get-ChildItem "." -Filter *.nuspec
$assetLinks = @()
foreach ($f in $files) {
    if ($f.Name -match '^(?<name>.*)\.nuspec$') {
        Write-Host "Found nuspec: Name:" $Matches.name
        $hostedFile = Find-Latest-Package-Version-File $Matches.name $Env:GV_SEMVER
        $assetLink = @{
            name      = "$($Matches.name).${Env:GV_SEMVER}.nupkg"
            url       = "https://gitlab.com/neftest/neftest-package-registry/-/package_files/$($hostedFile.id)/download"
            filepath  = "/binaries/$($Matches.name).${Env:GV_SEMVER}.nupkg"
            link_type = "package"
        }
        $assetLinks += $assetLink
    }
}

$releaseRequestBody = @{
    id          = "$CI_PROJECT_ID"
    name        = "$CI_PROJECT_TITLE v${Env:GV_SEMVER}"
    tag_name    = "v${Env:GV_SEMVER}"
    description = "" + (Get-Content ".\changelog\v${Env:GV_SEMVER}.md" -Raw)
    assets      = @{ links = $assetLinks }
}

$postJson = ($releaseRequestBody | ConvertTo-Json -Depth 10)


Write-Host "`n`n"
Write-Host $postJson

$_response = Invoke-WebRequest -Headers $headers -Method "POST" -ContentType "application/json" -Body $postJson -Uri "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"

Write-Host $([String]::new($_response.Content))

$footerLinks = @();
$footerLinks += "[Release Page](https://gitlab.com/${CI_PROJECT_PATH}/-/releases/v${Env:GV_SEMVER})"
$footerLinks += "[Documentation](https://neftest.gitlab.io/)"
$footerLinks += "[Source Code](https://gitlab.com/${CI_PROJECT_PATH}/-/tree/v${Env:GV_SEMVER})"

if ($Env:SHOW_NUGET_LINK) {
    $footerLinks += "[Nuget](https://www.nuget.org/packages/${CI_PROJECT_TITLE}/${Env:GV_SEMVER})"
}

$downloadLinks = @();
foreach($asset in $assetLinks) {
    $downloadLinks += "[$($asset.name)](https://gitlab.com/${CI_PROJECT_PATH}/-/releases/v${Env:GV_SEMVER}/downloads$($asset.filepath))"
}

$webhookBody = @{
    avatar_url = "https://gitlab.com/uploads/-/system/project/avatar/${CI_PROJECT_ID}/icon.png?width=64"
    embeds  = @(
        @{
            title        = "$CI_PROJECT_TITLE v${Env:GV_SEMVER} has been released!"
            url          = "https://gitlab.com/${CI_PROJECT_PATH}/-/releases/v${Env:GV_SEMVER}"
            color        = 10181046  #purple
            fields       = @(
                @{
                    name   = "Changelog"
                    value  = "" + (Get-Content ".\changelog\v${Env:GV_SEMVER}.md" -Raw)
                    inline = "false"
                }
                @{
                    name   = "Files"
                    value  = $downloadLinks -Join "`n"
                    inline = "false"
                }
                @{
                    name   = "Links"
                    value  = $footerLinks -Join " - "
                    inline = "true"
                }
            )
        }
    )
}

$webhookJson = ($webhookBody | ConvertTo-Json -Depth 10)

Write-Host "`n`n"
Write-Host $webhookJson
Write-Host "`n`n"


$_wresponse = Invoke-RestMethod -Method "POST" -ContentType "application/json" -Uri $Env:DISCORD_WEBHOOK -Body $webhookJson

Write-Host $([String]::new($_wresponse))