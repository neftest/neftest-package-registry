# NefTest Package Registry

A storage space for packages built from neftest project ci/cd.


## Pipeline Job Templates

Includes a few pipeline job templates that are meant to be used in different NefTest projects.

| Template  | Description |
|:------------- |:-------------|
| gitversion-template.yml | Provides a .pre job that users gitversion to generate project version info. |
| nuget-template.yml      | Provides .pre/.post/package/deploy jobs for working with nuget packages |
| release-template.yml    | Provides a deploy job that triggers a release (commits changelog / tags release / merges to release) |


## Package Life Cycle

TODO: Explain how packages are removed from the registry.
